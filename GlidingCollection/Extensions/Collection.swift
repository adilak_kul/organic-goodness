//
//  Collection.swift
//  GlidingCollection
//
//  Created by adilak on 1/5/2562 BE.
//  Copyright © 2562 Adilak Kulkanjanachiwin. All rights reserved.
//

import Foundation

// :nodoc:
extension Collection {
  subscript(safe index: Index) -> Iterator.Element? {
    return index >= startIndex && index < endIndex ? self[index] : nil
  }
}
