//
//  GlidingCollectionDataSource.swift
//  GlidingCollection
//
//  Created by adilak on 1/5/2562 BE.
//  Copyright © 2562 Adilak Kulkanjanachiwin. All rights reserved.
//

import Foundation

/// Datasource protocol of GlidingCollection.
public protocol GlidingCollectionDatasource {
  
  /// Number of items in vertical stack of items.
  ///
  /// - Parameter collection: GlidingCollection
  /// - Returns: number of items in stack
  func numberOfItems(in collection: GlidingCollection) -> Int
  
  /// Item at given index.
  ///
  /// - Parameters:
  ///   - collection: GlidingCollection
  ///   - index: index of item
  /// - Returns: item title
  func glidingCollection(_ collection: GlidingCollection, itemAtIndex index: Int) -> String
  
}
