//
//  CollectionCell.swift
//  GlidingCollection
//
//  Created by adilak on 1/5/2562 BE.
//  Copyright © 2562 Adilak Kulkanjanachiwin. All rights reserved.
//

import UIKit


class CollectionCell: UICollectionViewCell {
  
  @IBOutlet var imageView: UIImageView!
  
}
