//
//  GlidingViewController.swift
//  OrganicGoodness
//
//  Created by adilak on 1/5/2562 BE.
//  Copyright © 2562 Adilak Kulkanjanachiwin. All rights reserved.
//

import UIKit
import GlidingCollection

class GlidingViewController: UIViewController {
    
    @IBOutlet var glidingView: GlidingCollection!
    fileprivate var collectionView: UICollectionView!
    fileprivate var items = ["เมนูน้ำผักปั่น","เมนูน้ำปั่น", "เมนูสลัด", "การถนอมอาหาร"]
    fileprivate var images: [[UIImage?]] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
}

// MARK: - Setup
extension GlidingViewController {
    
    func setup() {
        setupGlidingCollectionView()
        loadImages()
    }
    
    private func setupGlidingCollectionView() {
        glidingView.dataSource = self as GlidingCollectionDatasource
        
        let nib = UINib(nibName: "CollectionCell", bundle: nil)
        collectionView = glidingView.collectionView
        collectionView.register(nib, forCellWithReuseIdentifier: "Cell")
        collectionView.delegate = self as UICollectionViewDelegate
        collectionView.dataSource = self as UICollectionViewDataSource
        collectionView.backgroundColor = glidingView.backgroundColor
    }
    
    private func loadImages() {
        for item in items {
            let imageURLs = FileManager.default.fileUrls(for: "jpeg", fileName: item)
            var images: [UIImage?] = []
            for url in imageURLs {
                guard let data = try? Data(contentsOf: url) else { continue }
                let image = UIImage(data: data)
                images.append(image)
            }
            self.images.append(images)
        }
    }
    
}

// MARK: - CollectionView 🎛
extension GlidingViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let section = glidingView.expandedItemIndex
        return images[section].count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? CollectionCell else { return UICollectionViewCell() }
        let section = glidingView.expandedItemIndex
        let image = images[section][indexPath.row]
        cell.imageView.image = image
        cell.contentView.clipsToBounds = true
        
        let layer = cell.layer
        let config = GlidingConfig.shared
        layer.shadowOffset = config.cardShadowOffset
        layer.shadowColor = config.cardShadowColor.cgColor
        layer.shadowOpacity = config.cardShadowOpacity
        layer.shadowRadius = config.cardShadowRadius
        
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let section = glidingView.expandedItemIndex
        let item = indexPath.item
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        
        let glidingDetailOneVC = (storyboard?.instantiateViewController(withIdentifier: "GlidingDetailOne"))!
        
        let glidingDetailTwoVC = (storyboard?.instantiateViewController(withIdentifier: "GlidingDetailTwo"))!
        let glidingDetailThreeVC = (storyboard?.instantiateViewController(withIdentifier: "GlidingDetailThree"))!
        let glidingDetailFourVC = (storyboard?.instantiateViewController(withIdentifier: "GlidingDetailFour"))!
        let glidingDetailFiveVC = (storyboard?.instantiateViewController(withIdentifier: "GlidingDetailFive"))!
        let glidingDetailSixVC = (storyboard?.instantiateViewController(withIdentifier: "GlidingDetailSix"))!
        let glidingDetailSevenVC = (storyboard?.instantiateViewController(withIdentifier: "GlidingDetailSeven"))!
        let glidingDetailEightVC = (storyboard?.instantiateViewController(withIdentifier: "GlidingDetailEight"))!
        let glidingDetailNineVC = (storyboard?.instantiateViewController(withIdentifier: "GlidingDetailNine"))!
        
        if section == 0 && item == 0 {
            self.present(glidingDetailOneVC, animated: true, completion: nil)
        }
        else if section == 0 && item == 1 {
            self.present(glidingDetailTwoVC, animated: true, completion: nil)
        }
        else if section == 1 && item == 0 {
            self.present(glidingDetailThreeVC, animated: true, completion: nil)
        }
        else if section == 1 && item == 1 {
            self.present(glidingDetailFourVC, animated: true, completion: nil)
        }
        else if section == 1 && item == 2 {
            self.present(glidingDetailFiveVC, animated: true, completion: nil)
        }
        else if section == 2 && item == 0 {
            self.present(glidingDetailSixVC, animated: true, completion: nil)
        }
        else if section == 2 && item == 1 {
            self.present(glidingDetailSevenVC, animated: true, completion: nil)
        }
        else if section == 3 && item == 0 {
            self.present(glidingDetailEightVC, animated: true, completion: nil)
        }
        else if section == 3 && item == 1 {
            self.present(glidingDetailNineVC, animated: true, completion: nil)
        }
//        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//        let glidingDetailOneVC = glidingStoryBoard.instantiateViewController(withIdentifier: "GlidingDetailOne") as! GlidingDetailViewController
//        let glidingDetailTwoVC = glidingStoryBoard.instantiateViewController(withIdentifier: "GlidingDetailTwo") as! GlidingDetailViewController
//
//        if section == 0 && item == 0 {
//            self.navigationController?.pushViewController(glidingDetailOneVC, animated: true)
//        }
//        else if section == 0 && item == 1 {
//            self.navigationController?.pushViewController(glidingDetailTwoVC, animated: true)
//        }
        
        print("Selected item #\(item) in section #\(section)")
        
    }
    
}

// MARK: - Gliding Collection 🎢
extension GlidingViewController: GlidingCollectionDatasource {
    
    func numberOfItems(in collection: GlidingCollection) -> Int {
        return items.count
    }
    
    func glidingCollection(_ collection: GlidingCollection, itemAtIndex index: Int) -> String {
        return items[index]
    }
    
}
