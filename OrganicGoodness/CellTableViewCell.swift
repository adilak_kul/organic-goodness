//
//  CellTableViewCell.swift
//  OrganicGoodness
//
//  Created by adilak on 1/01/2562 BE.
//  Copyright © 2562 Adilak Kulkanjanachiwin. All rights reserved.
//

import UIKit

class CellTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var pictureView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    //set up the call
    func configure(picture: UIImage, title: String) {
        
        pictureView.image = picture
        titleLabel.text = title
        
        pictureView.layer.cornerRadius = pictureView.bounds.width / 75.0
        pictureView.layer.masksToBounds = true
        pictureView.layer.borderColor = UIColor.white.cgColor
        pictureView.layer.borderWidth = 1.0

        
        cardView.layer.shadowColor = UIColor.gray.cgColor
        cardView.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        cardView.layer.shadowOpacity = 1.0
        cardView.layer.masksToBounds = false
        cardView.layer.cornerRadius = 3.0
        
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
