//
//  FoodCategory.swift
//  GoodEatins
//
//  Created by adilak on 10/1/2562 BE.
//  Copyright © 2562 Adilak Kulkanjanachiwin. All rights reserved.
//

import Foundation

struct FoodCategory {
    let title : String
    let imageName : String
}
