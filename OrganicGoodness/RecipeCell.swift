//
//  RecipeCell.swift
//  GoodEatins
//
//  Created by adilak kulkanjanachiwin on 8/24/18.
//  Copyright © 2018 adilak. All rights reserved.
//

import UIKit

class RecipeCell: UICollectionViewCell {
    
    @IBOutlet weak var recipeImg: UIImageView!
    @IBOutlet weak var recipeTitle: UILabel!
    @IBOutlet weak var visaulEffectView: UIVisualEffectView!
    @IBOutlet weak var colorRisk: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        recipeImg.layer.cornerRadius = 10
        visaulEffectView.layer.cornerRadius = 10
    }
    
    func configureCell(recipe: Recipe) {
        recipeImg.image = UIImage(named: recipe.imageName)
        recipeTitle.text = recipe.title
        colorRisk.image = UIImage(named: recipe.riskColorImage)
    }
}
