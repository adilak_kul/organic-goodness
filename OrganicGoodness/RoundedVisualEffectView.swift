//
//  RoundedVisualEffectView.swift
//  OrganicGoodness
//
//  Created by adilak on 26/2/2562 BE.
//  Copyright © 2562 Adilak Kulkanjanachiwin. All rights reserved.
//

import UIKit

class RoundedVisualEffectView: UIVisualEffectView {

    override func awakeFromNib() {
        self.layer.cornerRadius = 10
        self.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMinYCorner]
        self.clipsToBounds = true
        
    }

}
