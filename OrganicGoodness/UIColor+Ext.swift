//
//  UIColor+Ext.swift
//  Cafegram2EN
//
//  Created by adilak on 10/1/2562 BE.
//  Copyright © 2562 Adilak Kulkanjanachiwin. All rights reserved.
//

import UIKit

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        let redValue = CGFloat(red) / 255.0
        let greenValue = CGFloat(green) / 255.0
        let blueValue = CGFloat(blue) / 255.0
        
        self.init(red: redValue, green: greenValue, blue: blueValue, alpha: 1.0)
    }
}
