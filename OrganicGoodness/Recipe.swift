//
//  Recipe.swift
//  GoodEatins
//
//  Created by adilak on 10/1/2562 BE.
//  Copyright © 2562 Adilak Kulkanjanachiwin. All rights reserved.
//

import Foundation

struct Recipe {
    var title: String
    var instructions: String
    var imageName: String
    var priceList: String
    var riskDetail: String
    var riskColorImage : String
    var price1: String
    var price2: String
    var store1: String
    var store2: String
    var detail: String
    var detail2: String
}
