//
//  Cafe.swift
//  Cafegram2EN
//
//  Created by adilak on 10/1/2562 BE.
//  Copyright © 2562 Adilak Kulkanjanachiwin. All rights reserved.
//

import Foundation

class Cafe {
    var name: String
    var type: String
    var location: String
    var image: String
    var phone: String
    var description: String
    var isVisited: Bool
    var distance: String
    
    init(name: String, type: String, location: String, image: String, phone: String, description: String, isVisited: Bool, distance: String) {
        self.name = name
        self.type = type
        self.location = location
        self.image = image
        self.phone = phone
        self.description = description
        self.isVisited = isVisited
        self.distance = distance
    }
    
    convenience init() {
        self.init(name: "", type: "", location: "", image: "", phone: "", description: "", isVisited: false, distance: "")
    }
}
