//
//  OnboardingViewController.swift
//  OrganicGoodness
//
//  Created by adilak on 25/4/2562 BE.
//  Copyright © 2562 Adilak Kulkanjanachiwin. All rights reserved.
//

import UIKit
import PaperOnboarding

class OnboardingViewController: UIViewController, PaperOnboardingDataSource, PaperOnboardingDelegate {

    @IBOutlet weak var onboardingView: OnboardingView!
    
    @IBOutlet weak var getStartedButton: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        onboardingView.dataSource = self
        onboardingView.delegate = self
        
        
        for attribute: NSLayoutConstraint.Attribute in [.left, .right, .top, .bottom] {
            let constraint = NSLayoutConstraint(item: onboardingView,
                                                attribute: attribute,
                                                relatedBy: .equal,
                                                toItem: view,
                                                attribute: attribute,
                                                multiplier: 1,
                                                constant: 0)
            view.addConstraint(constraint)
        }
    }
    
    func onboardingItemsCount() -> Int {
        return 3
    }
    
    func onboardingItem(at index: Int) -> OnboardingItemInfo {
        let backgroundColorOne = UIColor(red: 191/255, green: 58/255, blue: 99/255, alpha: 1)
        let backgroundColorTwo = UIColor(red: 58/255, green: 191/255, blue: 150/255, alpha: 1)
        let backgroundColorThree = UIColor(red: 224/255, green: 153/255, blue: 175/255, alpha: 1)
        
        let titleFont = UIFont(name: "AvenirNext-Bold", size: 24)!
        let descriptionFont = UIFont(name: "AvenirNext-Regular", size: 18)!
        
        return [OnboardingItemInfo(informationImage: UIImage(named: "onboard-healthy01")!,
                                   title: "จะเริ่มดูแลสุขภาพตั้งแต่ตอนนี้ หรือจะรอเป็นโรคก่อนแล้วค่อยรักษา?",
                                   description: "คนไทยเสียชีวิตจากโรคมะเร็ง 50,000 คนต่อปี เกิดจากการใช้ชีวิตประจำวันแบบผิดๆ ของคนในยุคสมัยใหม่ หนึ่งในนั้นคือการบริโภคอาหาร",
                                   pageIcon: UIImage(named: "onboard-healthy02")!,
                                   color: backgroundColorOne,
                                   titleColor: UIColor.white,
                                   descriptionColor: UIColor.white,
                                   titleFont: titleFont,
                                   descriptionFont: descriptionFont),
                
                OnboardingItemInfo(informationImage: UIImage(named: "onboard-healthy02")!,
                                   title: "เกษตรอินทรีย์คืออะไร",
                                   description: "เกษตรอินทรีย์คือระบบการผลิตที่ปลอดจากการใช้สารเคมีทุกชนิดทุกขั้นตอน ตั้งแต่จัดพื้นที่การเพาะปลูกที่ห่างไกลจากแหล่งมลภาวะ การเตรียมดิน การใส่ปุ๋ย การรดน้ำ และการขนส่ง",
                                   pageIcon: UIImage(named: "onboard-healthy01")!,
                                   color: backgroundColorTwo,
                                   titleColor: UIColor.white,
                                   descriptionColor: UIColor.white,
                                   titleFont: titleFont,
                                   descriptionFont: descriptionFont),
                
                OnboardingItemInfo(informationImage: UIImage(named: "onboard-healthy01")!,
                                   title: "ส่งเสริมให้คนไทยหันมาดูแลสุขภาพ",
                                   description: "ด้วยการบริโภคสินค้าเกษตรอินทรีย์ ปราศจากสารเคมีตกค้าง 100% ดีต่อทุกฝ่ายทั้งผู้บริโภค เกษตรกร และสิ่งแวดล้อม สดใหม่ปลูกตามฤดูกาล ดีต่อสุขภาพ ลดความเสี่ยงในการเกิดโรครักษาระบบนิเวศ",
                                   pageIcon: UIImage(named: "onboard-healthy02")!,
                                   color: backgroundColorThree,
                                   titleColor: UIColor.white,
                                   descriptionColor: UIColor.white,
                                   titleFont: titleFont,
                                   descriptionFont: descriptionFont)][index]
    }
    
    func onboardingConfigurationItem(_: OnboardingContentViewItem, index _: Int) {
        
    }
    
    func onboardingWillTransitonToIndex(_ index: Int) {
        if index == 1 {
            
            if self.getStartedButton.alpha == 1 {
                UIView.animate(withDuration: 0.2, animations: {
                    self.getStartedButton.alpha = 0
                })
            }
        }
    }
    
    func onboardingDidTransitonToIndex(_ index: Int) {
        if index == 2 {
            UIView.animate(withDuration: 0.4, animations: {
                self.getStartedButton.alpha = 1
            })
        }
    }

    @IBAction func gotStarted(_ sender: Any) {
        let userDefaults = UserDefaults.standard
        
        userDefaults.set(true, forKey: "onboardingComplete")
        
        userDefaults.synchronize()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
