//
//  cameraVC.swift
//  OrganicGoodness
//
//  Created by adilak on 26/2/2562 BE.
//  Copyright © 2562 Adilak Kulkanjanachiwin. All rights reserved.
//

import UIKit
import VisualRecognitionV3
import SVProgressHUD
import NVActivityIndicatorView

class cameraVC: UIViewController, NVActivityIndicatorViewable {
    
    let imagePicker = UIImagePickerController()
    var classificationResults : [String] = []
    
    let apiKey = "qcLSeXZLSyr4YhHZgYne2IlO2NKzjr28mSD2eWdow7Tv"
    let version = "2019-02-17"
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var classificationLabel: UILabel!
    @IBOutlet weak var roundedShadowView: RoundedShadowView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func cameraButtonPresed(_ sender: UIBarButtonItem) {
        
        guard UIImagePickerController.isSourceTypeAvailable(.camera) else {
            presentPhotoPicker(sourceType: .photoLibrary)
            return
        }
        
        let photoSourcePicker = UIAlertController()
        let takePhotoAction = UIAlertAction(title: "Take Photo", style: .default) { _ in
            self.presentPhotoPicker(sourceType: .camera)
        }
        
        let choosePhotoAction = UIAlertAction(title: "Choose Photo", style: .default) { _ in
            self.presentPhotoPicker(sourceType: .photoLibrary)
        }
        
        photoSourcePicker.addAction(takePhotoAction)
        photoSourcePicker.addAction(choosePhotoAction)
        photoSourcePicker.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        present(photoSourcePicker, animated: true, completion: nil)
    }
    
    func presentPhotoPicker(sourceType: UIImagePickerController.SourceType) {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = sourceType
        present(picker, animated: true, completion: nil)
    }
    
    @IBAction func infoButtonPressed(_ sender: UIButton) {
        
        performSegue(withIdentifier: "infoSegue", sender: self)
        
    }
    
    
}

extension cameraVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        
//        SVProgressHUD.show()
        let size = CGSize(width: 30.0, height: 30.0)
        
        startAnimating(size, message: "Loading", type: NVActivityIndicatorType.ballGridPulse, color: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), textColor: #colorLiteral(red: 0, green: 0.7559493184, blue: 0.5889989734, alpha: 1), fadeInAnimation: nil)
        
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            
            imageView.image = image
            
            imagePicker.dismiss(animated: true, completion: nil)
            
            let visualRecognition = VisualRecognition(version: version, apiKey: apiKey)
            
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            
            let imageData = image.jpegData(compressionQuality: 0.01)
            
            let fileURL = documentsURL.appendingPathComponent("tempImage.jpg")
            
            ((try? imageData?.write(to: fileURL, options: [])) as ()??)
            
            visualRecognition.classify(imagesFile: fileURL) { response, error in
                if let error = error {
                    print(error)
                }
                guard let classifiedImages = response?.result else {
                    print("Failed to classify the image")
                    return
                }
                let classes = classifiedImages.images.first!.classifiers.first!.classes
                
                self.classificationResults = []
                
                for index in 0..<classes.count {
                    self.classificationResults.append(classes[index].className)
                }
                print(self.classificationResults)
                
                DispatchQueue.main.async {
//                    SVProgressHUD.dismiss()
//                    self.infoButton.isHidden = false
                    self.stopAnimating()
                }
                
                if self.classificationResults.contains("apple") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "แอปเปิ้ล"
                    }
                }
                    
                else if self.classificationResults.contains("tomato") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "มะเขือเทศ"
                    }
                }
                    
                else if self.classificationResults.contains("orange") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "ส้ม"
                    }
                }
                    
                else if self.classificationResults.contains("onion") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "หัวหอม"
                    }
                }
                    
                else if self.classificationResults.contains("broccoli") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "บล็อคโคลี่"
                    }
                }
                    
                else if self.classificationResults.contains("watermalon") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "แตงโม"
                    }
                }
                    
                else if self.classificationResults.contains("carrot") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "แครอท"
                    }
                }
                    
                else if self.classificationResults.contains("asparagus") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "หน่อไม้ฝรั่ง"
                    }
                }
                    
                else if self.classificationResults.contains("cauliflower") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "กะหล่ำดอก"
                    }
                }
                    
                else if self.classificationResults.contains("potato") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "มันฝรั่ง"
                    }
                }
                    
                else if self.classificationResults.contains("garlic") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "กระเทียม"
                    }
                }
                    
                else if self.classificationResults.contains("cucumber") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "แตงกวา"
                    }
                }
                    
                else if self.classificationResults.contains("bell pepper") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "พริกหยวก"
                    }
                }
                else if self.classificationResults.contains("lemongrass") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "ตะไคร้"
                    }
                }
                else if self.classificationResults.contains("cabbage") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "กะหล่ำปลี"
                    }
                }
                else if self.classificationResults.contains("bean sprouts") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "ถั่วงอก"
                    }
                }
                else if self.classificationResults.contains("baby corn") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "ข้าวโพดฝักอ่อน"
                    }
                }
                else if self.classificationResults.contains("celery") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "คื่นฉ่ายฝรั่ง"
                    }
                }
                else if self.classificationResults.contains("chinese kale") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "ผักคะน้า"
                    }
                }
                else if self.classificationResults.contains("chinese cabbage") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "ผักกาดขาว"
                    }
                }
                else if self.classificationResults.contains("chinese parsley") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "ผักชีจีน"
                    }
                }
                else if self.classificationResults.contains("parsley") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "พาร์สลีย์"
                    }
                }
                else if self.classificationResults.contains("cowpea") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "ถั่วฝักยาว"
                    }
                }
                else if self.classificationResults.contains("eggplant") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "มะเขือ"
                    }
                }
                else if self.classificationResults.contains("enoki mushroom") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "เห็ดเข็มทอง"
                    }
                }
                else if self.classificationResults.contains("galangal") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "ข่า"
                    }
                }
                else if self.classificationResults.contains("green pepper") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "พริกหยวก"
                    }
                }
                else if self.classificationResults.contains("guinea pepper") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "พริกขี้หนู"
                    }
                }
                else if self.classificationResults.contains("jew’s ear") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "เห็ดหูหนู"
                    }
                }
                else if self.classificationResults.contains("morning glory") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "ผักบุ้ง"
                    }
                }
                else if self.classificationResults.contains("lettuce") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "ผักกาดหอม"
                    }
                }
                else if self.classificationResults.contains("lime") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "มะนาว"
                    }
                }
                else if self.classificationResults.contains("lemon") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "เลมอน"
                    }
                }
                else if self.classificationResults.contains("peas") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "ถั่วลันเตา"
                    }
                }
                else if self.classificationResults.contains("pumpkins") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "ฟักทอง"
                    }
                }
                else if self.classificationResults.contains("radish") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "หัวไชเท้า"
                    }
                }
                else if self.classificationResults.contains("shiitake mushroom") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "เห็ดหอม"
                    }
                }
                else if self.classificationResults.contains("shallot") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "หอมแดง"
                    }
                }
                else if self.classificationResults.contains("spinach") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "ผักปวยเล้ง"
                    }
                }
                else if self.classificationResults.contains("amaranth") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "ผักโขม"
                    }
                }
                else if self.classificationResults.contains("sweet basil") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "โหระพา"
                    }
                }
                else if self.classificationResults.contains("taro") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "เผือก"
                    }
                }
                else if self.classificationResults.contains("thai Basil") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "โหระพา"
                    }
                }
                else if self.classificationResults.contains("papaya") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "มะละกอ"
                    }
                }
                else if self.classificationResults.contains("green bean") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "ถั่วเขียว"
                    }
                }
                else if self.classificationResults.contains("soybean") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "ถั่วเหลือง"
                    }
                }
                else if self.classificationResults.contains("water minosa") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "ผักกระเฉด"
                    }
                }
                else if self.classificationResults.contains("avocado") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "อะโวคาโด"
                    }
                }
                else if self.classificationResults.contains("bok choy") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "ผักกวางตุ้งไต้หวัน"
                    }
                }
                else if self.classificationResults.contains("ginger") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "ขิง"
                    }
                }
                else if self.classificationResults.contains("cilantro") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "ผักชี"
                    }
                }
                else if self.classificationResults.contains("east India spinach") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "ผักตำลึง"
                    }
                }
                else if self.classificationResults.contains("green onions") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "ต้นหอม"
                    }
                }
                else if self.classificationResults.contains("napa cabbage") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "ผักกาดขาว"
                    }
                }
                else if self.classificationResults.contains("peppermint") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "สะระแหน่"
                    }
                }
                else if self.classificationResults.contains("red cabbage") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "กะหล่ำปลีแดง"
                    }
                }
                else if self.classificationResults.contains("zucchini") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "ซูกินี"
                    }
                }
                else if self.classificationResults.contains("spur pepper") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "พริกชี้ฟ้า"
                    }
                }
                else if self.classificationResults.contains("chilli Padi") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "พริกขี้หนู"
                    }
                }
                else if self.classificationResults.contains("thai pepper") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "พริกขี้หนู"
                    }
                }
                else if self.classificationResults.contains("winter melon") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "ฟักเขียว หรือ ฟักแฟง"
                    }
                }
                else if self.classificationResults.contains("coconut") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "มะเขือ"
                    }
                }
                else if self.classificationResults.contains("blackberry") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "แบล็คเบอร์รี่"
                    }
                }
                else if self.classificationResults.contains("banana") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "กล้วย"
                    }
                }
                else if self.classificationResults.contains("blueberry") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "บลูเบอร์รี่"
                    }
                }
                else if self.classificationResults.contains("cherry") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "เชอร์รี่"
                    }
                }
                else if self.classificationResults.contains("dragon fruit") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "แก้วมังกร"
                    }
                }
                else if self.classificationResults.contains("durian") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "ทุเรียน"
                    }
                }
                else if self.classificationResults.contains("grape") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "องุ่น"
                    }
                }
                else if self.classificationResults.contains("grapefruit") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "เกรปฟรุต"
                    }
                }
                else if self.classificationResults.contains("guava") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "ฝรั่ง"
                    }
                }
                else if self.classificationResults.contains("jackfruit") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "ขนุน"
                    }
                }
                else if self.classificationResults.contains("kiwi") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "กีวี่"
                    }
                }
                else if self.classificationResults.contains("longan") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "ลำไย"
                    }
                }
                else if self.classificationResults.contains("lychee") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "ลิ้นจี่"
                    }
                }
                else if self.classificationResults.contains("mandarin orange") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "ส้มแมนดาริน"
                    }
                }
                else if self.classificationResults.contains("mangosteen") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "มังคุด"
                    }
                }
                else if self.classificationResults.contains("passionfruit") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "เสาวรส"
                    }
                }
                else if self.classificationResults.contains("peach") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "ท้อ หรือ พีช"
                    }
                }
                else if self.classificationResults.contains("pineapple") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "สับปะรด"
                    }
                }
                else if self.classificationResults.contains("rambutan") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "เงาะ"
                    }
                }
                else if self.classificationResults.contains("rose apple") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "ชมพู่"
                    }
                }
                else if self.classificationResults.contains("strawberry") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "สตรอเบอร์รี่"
                    }
                }
                else if self.classificationResults.contains("Raspberry") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "แรสเบอร์รี่ หรือ ราสเบอร์รี่ "
                    }
                }
                else if self.classificationResults.contains("pomegranate") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "ทับทิม"
                    }
                }
                else if self.classificationResults.contains("cantaloupe") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "แคนตาลูป"
                    }
                }
                else if self.classificationResults.contains("mango") {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "มะม่วง"
                    }
                }
                    
                else {
                    DispatchQueue.main.async {
                        self.classificationLabel.text = "I'm not sure  what this is. Please try again."
                    }
                }
            }
        }
    }
}
