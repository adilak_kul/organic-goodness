//
//  CafeTableViewController.swift
//  Cafegram2EN
//
//  Created by adilak on 28/3/2562 BE.
//  Copyright © 2562 Adilak Kulkanjanachiwin. All rights reserved.
//

import UIKit

class CafeTableViewController: UITableViewController {
    
    var cafes: [Cafe] = [
        Cafe(name: "บิ๊กซี สาขารังสิต", type: "ห้างสรรพสินค้า", location: "94 Phahonyothin Road Prachathipat, Thanyaburi Pathum Thani 12130 Thailand", image: "avatar1", phone: "02 958 0100", description: "เปิดทุกวัน ตั้งแต่ 09:00-23:00", isVisited: false, distance: ""),
        
        Cafe(name: "Food Hall Siam Paragon", type: "ห้างสรรพสินค้า", location: "991 Rama I Rd, Khwaeng Pathum Wan, Khet Pathum Wan, Krung Thep Maha Nakhon 10330", image: "avatar2", phone: "095 659 5310", description: "เปิดทุกวัน ตั้งแต่ 10:00-22:00", isVisited: false, distance: ""),
        
        Cafe(name: "Food Land (ลาดพร้าว)", type: "ห้างสรรพสินค้า", location: "2675 ซอย ลาดพร้าว 95 แขวง คลองเจ้าคุณสิงห์ เขต วังทองหลาง กรุงเทพมหานคร 10310", image: "avatar3", phone: "02 530 0220", description: "เปิดทุดวัน 24 ชั่วโมง", isVisited: false, distance: ""),
        
        Cafe(name: "Tops Market Esplanade Ratchadaphisek", type: "ห้างสรรพสินค้า", location: "99 ถนนรัชดาภิเษก แขวง ดินแดง เขต ดินแดง กรุงเทพมหานคร 10400", image: "avatar4", phone: " 02 660 9092", description: "เปิดทุกวัน ตั้งแต่ 10:00-22:00", isVisited: false, distance: ""),
        
        
        Cafe(name: "ท็อปส์ มาร์เก็ตเพลส (เซ็นทรัลชิดลม)", type: "ห้างสรรพสินค้า", location: "29 ซอย ชิดลม แขวง ลุมพินี เขต ปทุมวัน กรุงเทพมหานคร 10330", image: "avatar5", phone: "-", description: "เปิดทุกวัน ตั้งแต่ 10:00-22:00", isVisited: false, distance: ""),
        
        Cafe(name: "ท็อปส์ มาร์เก็ต เซ็นทรัล พลาซ่า ลาดพร้าว", type: "ห้างสรรพสินค้า", location: "1693 ถนน พหลโยธิน แขวง ลาดยาว เขต จตุจักร กรุงเทพมหานคร 10900", image: "avatar6", phone: "02 937 1700", description: "เปิดทุกวัน ตั้งแต่ 10:00-22:00", isVisited: false, distance: ""),
        
        Cafe(name: "Lemon Farm แจ้งวัฒนะ", type: "ห้างสรรพสินค้า", location: "104/34 The Avenue, Chaengwatthana Rd, แขวง ทุ่งสองห้อง เขต หลักสี่ กรุงเทพมหานคร 10210", image: "avatar7", phone: " 02 575 2222", description: "เปิดทุกวัน ตั้งแต่ 09:00-21:00 ", isVisited: false, distance: ""),
        
        Cafe(name: "วิลล่า มาร์เก็ต สีลม", type: "ห้างสรรพสินค้า", location: "425/, Silom Rd - ซอย สีลม 7, แขวง สีลม เขต บางรัก กรุงเทพมหานคร 10500", image: "avatar8", phone: "02 636 6859", description: "เปิดทุกวัน ตั้งแต่ 07:00-21:00", isVisited: false, distance: ""),
        
        Cafe(name: "วิลล่ามาร์เก็ตสัมมากร", type: "ห้างสรรพสินค้า", location: "86 ถนน รามคำแหง แขวง สะพานสูง เขต สะพานสูง กรุงเทพมหานคร 10240", image: "avatar9", phone: "02 636 6859", description: "เปิดทุกวัน ตั้งแต่ 07:00-21:00", isVisited: false, distance: ""),
        
        
        Cafe(name: "JJ Mall", type: "ห้างสรรพสินค้า", location: "588 ถนนกำแพงเพชร 2 แขวงจตุจักร เขตจตุจักร กรุงเทพมหานคร 10900 ประเทศไทย", image: "avatar10", phone: "086 321 1030", description: "เปิดทุกวัน ตั้งแต่ 06:00-22:00", isVisited: false, distance: ""),
        
        
        Cafe(name: "ร้านโป๊ะผัก", type: "ร้านค้า", location: "29 ถนนพรานนก บางกอกน้อย กรุงเทพมหานคร 10700 ประเทศไทย", image: "avatar11", phone: "02 866 1719", description: "เปิดทุกวัน ตั้งแต่ 10:00-19:00", isVisited: false, distance: ""),
        
        
        Cafe(name: "ร้านโป๊ะผัก", type: "ร้านค้า", location: "118/41 หมู่4 ซอยติวานนท์14 ถนนติวานนท์ ตำบลตลาดขวัญ อำเภอเมือง นนทบุรี 11000", image: "avatar12", phone: "02 265 9999", description: "เปิดทุกวัน ตั้งแต่ 10:00-19:00", isVisited: false, distance: ""),
        
        
        Cafe(name: "Golden Place", type: "ร้านค้า", location: "เลขที่ 185 ถนน ประดิษฐ์มนูธรรม แขวง วังทองหลาง เขต วังทองหลาง กรุงเทพมหานคร 10310", image: "avatar13", phone: "02 935 6000", description: "เปิดทุกวัน ตั้งแต่ 07:00-22:00", isVisited: false, distance: ""),
        
        Cafe(name: "บ้านนาวิลิต", type: "ร้านค้า", location: "183 ถนน ราชดำริ แขวง ลุมพินี เขต ปทุมวัน กรุงเทพมหานคร 10330", image: "avatar14", phone: "02 651 9779", description: "เปิดจ-ศ ตั้งแต่ 08:30-17:00 ", isVisited: false, distance: ""),
        
        Cafe(name: "บ้านขนมนันทวัน", type: "ร้านค้า", location: "607 Thanon Phet Kasem อ.เมืองเพชรบุรี จ.เพชรบุรี 76000 ประเทศไทย", image: "avatar15", phone: "032 419 910", description: "เปิดทุกวัน ตั้งแต่ 07:00-20:00", isVisited: false, distance: ""),
        
        
        Cafe(name: "Baimiang Healthy Shop สาขาเดอะ เซอร์เคิล ราชพฤกษ์" , type: "ร้านค้า", location: "M24 The Circle Ratchapruk, ถนน ราชพฤกษ์ Bangramad, เขต ตลิ่งชัน กรุงเทพมหานคร 10170", image: "avatar16", phone: "084 387 7160", description: "เปิดทุกวัน ตั้งแต่ 09:30-21:30", isVisited: false, distance: ""),
        
        
        Cafe(name: "Baimiang Healthy ShopสาขาRain Hill", type: "ร้านค้า", location: "M24 The Circle Ratchapruk, ถนน ราชพฤกษ์ Bangramad, เขต ตลิ่งชัน กรุงเทพมหานคร 10170", image: "avatar17", phone: "063 212 0367", description: "เปิดทุกวัน ตั้งแต่ 09:00-21:00", isVisited: false, distance: ""),
        
        
        Cafe(name: "Baimiang Healthy Shopนวมินทร์ ซิตี้ อเวนิว", type: "ร้านค้า", location: "291/1 ถนน ประเสริฐมนูกิจ Jorakay Bua, เขต ลาดพร้าว กรุงเทพมหานคร 10230", image: "avatar18", phone: "063 212 0375", description: "เปิดทุกวัน ตั้งแต่ 09:00-21:00", isVisited: false, distance: ""),
        
        
        Cafe(name: "Baimiang Healthy Shop ดิอัพ พระราม 3", type: "ร้านค้า", location: "54 ถนน นราธิวาสราชนครินทร์ แขวง ช่องนนทรี เขต ยานนาวา กรุงเทพมหานคร 10120", image: "avatar19", phone: "063 212 0398", description: "เปิดทุกวัน ตั้งแต่ 11:00-21:00", isVisited: false, distance: ""),
        
        Cafe(name: "Baimiang Healthy Shop โฮมโปร พระราม 2", type: "ร้านค้า", location: "7ELEVEn, 45/581 Moo 6 (Next to, แขวง แสมดำ เขต บางขุนเทียน กรุงเทพมหานคร 10150", image: "avatar20", phone: "063 212 0377", description: "เปิดทุกวัน ตั้งแต่ 09:00-21:00", isVisited: false, distance: ""),
        
        Cafe(name: "ข้าวกล้อง", type: "ร้านค้า", location: "3077/51 ถนนสุขุมวิท แขวง บางจาก เขต พระโขนง กรุงเทพมหานคร 10260", image: "avatar21", phone: "02 747 9379", description: "เปิดทุกวัน ตั้งแต่ 08:00-20:00", isVisited: false, distance: ""),
    ]

    // MARK:- View controller life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.prefersLargeTitles = true
        
        // Customize the navigation bar
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        if let customFont = UIFont(name: "Rubik-Medium", size: 40.0) {
            // For Xcode 9 users, NSAttributedString.Key is equal to NSAttributedStringKey
            navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(red: 255.0/255.0, green: 0.0/255.0, blue: 153.0/255.0, alpha: 1), NSAttributedString.Key.font: customFont]
        }
        
        navigationController?.hidesBarsOnSwipe = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.hidesBarsOnSwipe = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK:- UITableViewDataSource Protocol

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return cafes.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "Cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! CafeTableViewCell

        // Configure the cell...
        cell.nameLabel.text = cafes[indexPath.row].name
        cell.locationLabel.text = cafes[indexPath.row].location
        cell.typeLabel.text = cafes[indexPath.row].type
        cell.thumbnailImageView.image = UIImage(named: cafes[indexPath.row].image)
        
        cell.accessoryType = cafes[indexPath.row].isVisited ? .checkmark : .none

        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    // MARK:- UITableViewDelegate Protocol
        
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { (action, sourceView, completionHandler) in
            // Delete the row from the data source
            self.cafes.remove(at: indexPath.row)
            
            self.tableView.deleteRows(at: [indexPath], with: .fade)
            
            // Call completion handler to dismiss the action button
            completionHandler(true)
        }
        
        let shareAction = UIContextualAction(style: .normal, title: "Share") { (action, sourceView, completionHandler) in
            let defaultText = "Just checking in at " + self.cafes[indexPath.row].name
            
            let activityController: UIActivityViewController
            
            if let imageToShare = UIImage(named: self.cafes[indexPath.row].image) {
                activityController = UIActivityViewController(activityItems: [defaultText, imageToShare], applicationActivities: nil)
            } else {
                activityController = UIActivityViewController(activityItems: [defaultText], applicationActivities: nil)
            }
            
            self.present(activityController, animated: true, completion: nil)
            completionHandler(true)
        }
        
        deleteAction.backgroundColor = UIColor(red: 231.0/255.0, green: 76.0/255.0, blue: 60.0/255.0, alpha: 1.0)
        deleteAction.image = UIImage(named: "delete")
        
        shareAction.backgroundColor = UIColor(red: 254.0/255.0, green: 149.0/255.0, blue: 38.0/255.0, alpha: 1.0)
        shareAction.image = UIImage(named: "share")
        
        let swipeConfiguration = UISwipeActionsConfiguration(actions: [deleteAction, shareAction])
        
        return swipeConfiguration
    }
    
    override func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let checkInAction = UIContextualAction(style: .normal, title: "Check-in") { (action, sourceView, completionHandler) in
            let cell = tableView.cellForRow(at: indexPath) as! CafeTableViewCell
            self.cafes[indexPath.row].isVisited = (self.cafes[indexPath.row].isVisited) ? false : true
            cell.accessoryType = (self.cafes[indexPath.row].isVisited) ? .checkmark : .none
            
            completionHandler(true)
        }
        
        // Customize the action button
        checkInAction.backgroundColor = UIColor(red: 39, green: 174, blue: 96)
        
        checkInAction.image = self.cafes[indexPath.row].isVisited ? UIImage(named: "undo") : UIImage(named: "tick")
        
        let swipeConfiguration = UISwipeActionsConfiguration(actions: [checkInAction])
        
        return swipeConfiguration
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showCafeDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let destinationController = segue.destination as! CafeDetailViewController
                destinationController.cafe = cafes[indexPath.row]
            }
        }
    }


}
