//
//  RecipeDetailVC.swift
//  GoodEatins
//
//  Created by adilak on 10/1/2562 BE.
//  Copyright © 2562 Adilak Kulkanjanachiwin. All rights reserved.
//

import UIKit

class RecipeDetailVC: UIViewController {
    
    @IBOutlet weak var store1: UILabel!
    @IBOutlet weak var price1: UILabel!
    @IBOutlet weak var store2: UILabel!
    @IBOutlet weak var price2: UILabel!
    
    @IBOutlet weak var detail: UILabel!
    @IBOutlet weak var priceViewOne: UIView!
    @IBOutlet weak var priceViewTwo: UIView!
    @IBOutlet weak var head: UIView!

    @IBOutlet weak var detail2: UILabel!
    @IBOutlet weak var recipeImg: UIImageView!
    
    @IBOutlet weak var rdSegment: UISegmentedControl!
    @IBOutlet weak var firstTextView: UITextView!
    @IBOutlet weak var thirdTextView: UITextView!
    
    
    
    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var secondView: UIView!
    @IBOutlet weak var thirdView: UIView!
    
    var selectedRecipe: Recipe!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        recipeImg.image = UIImage(named: selectedRecipe.imageName)
        self.navigationItem.title = selectedRecipe.title
        self.firstTextView.text = selectedRecipe.instructions
        thirdTextView.isHidden = true
        priceViewOne.isHidden = true
        priceViewTwo.isHidden = true
        
        priceViewOne.layer.shadowColor = UIColor.gray.cgColor
        priceViewOne.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        priceViewOne.layer.shadowOpacity = 1.0
        priceViewOne.layer.masksToBounds = false
        priceViewOne.layer.cornerRadius = 3.0
        priceViewTwo.layer.shadowColor = UIColor.gray.cgColor
        priceViewTwo.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        priceViewTwo.layer.shadowOpacity = 1.0
        priceViewTwo.layer.masksToBounds = false
        priceViewTwo.layer.cornerRadius = 3.0
    }
    @IBAction func rdSegmentTapped(_ sender: Any) {
        
//        if (sender as AnyObject).selectedSegmentIndex == 0{
//            thirdTextView.isHidden = true
//            firstView.alpha =  1
//            secondView.alpha = 0
//            thirdView.alpha = 0
//            firstTextView.alpha = 1
//            thirdView.alpha = 0
//            firstTextView.text = selectedRecipe.instructions
//
//        }
//        else if (sender as AnyObject).selectedSegmentIndex == 1 {
//            firstView.alpha =  0
//            secondView.alpha = 1
//            thirdView.alpha = 0
//            firstTextView.alpha = 0
//            thirdView.alpha = 0
//            firstTextView.isHidden = true
//            thirdTextView.isHidden = true
//        }
//        else{
//            firstView.alpha =  0
//            secondView.alpha = 0
//            thirdView.alpha = 1
//            firstTextView.alpha = 0
//            thirdView.alpha = 1
//            thirdTextView.text = selectedRecipe.riskDetail
//            firstTextView.isHidden = true
//        }
        
        let getIndex = rdSegment.selectedSegmentIndex
        print(getIndex)

        switch (getIndex) {
        case 0:
            
            firstTextView.alpha = 1
            thirdTextView.alpha = 0
            priceViewOne.alpha = 0
            priceViewTwo.alpha = 0
            //            head.alpha = 0
            self.firstTextView.text = selectedRecipe.instructions
            
        case 1:
        
            priceViewOne.alpha = 1
            priceViewTwo.alpha = 1
            //            head.alpha = 1
            //            head.isHidden = false
            priceViewOne.isHidden = false
            priceViewTwo.isHidden = false
            firstTextView.alpha = 0
            thirdTextView.alpha = 0
            self.price1.text = selectedRecipe.price1
            self.price2.text = selectedRecipe.price2
            self.store1.text = selectedRecipe.store1
            self.store2.text = selectedRecipe.store2
            self.detail.text = selectedRecipe.detail
            self.detail2.text = selectedRecipe.detail2
            
        case 2:
            priceViewOne.alpha = 0
            priceViewTwo.alpha = 0
            //            head.alpha = 0
            firstTextView.alpha = 1
            thirdTextView.alpha = 0
            self.firstTextView.text = selectedRecipe.riskDetail
        default:
            print("no select")
        }
        
        
        
    }
}
