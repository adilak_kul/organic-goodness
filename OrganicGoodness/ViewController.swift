//
//  ViewController.swift
//  CustomPinsMap
//
//  Created by adilak on 1/01/2562 BE.
//  Copyright © 2562 Adilak Kulkanjanachiwin. All rights reserved.
//

import UIKit
import PaperOnboarding
import CollectionViewSlantedLayout

class ViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewLayout: CollectionViewSlantedLayout!
    
    internal var covers = [[String: String]]()
    let tiltles : [String] = ["เกษตรอินทรีย์คืออะไร", "ผักปลอดสารพิษ vs ผักเกษตรอินทรีย์", "ทำไมต้องบริโภคสินค้าเกษตรอินทรีย์", "พยากรณ์ NCDs", "พยากรณ์ NCDs", "พยากรณ์ NCDs", "พยากรณ์ NCDs", "พยากรณ์ NCDs", "พยากรณ์ NCDs", "พยากรณ์ NCDs","พยากรณ์ NCDs", "พยากรณ์ NCDs"]
    
    let reuseIdentifier = "customViewCell"
    
    
    
    
    override func loadView() {
        super.loadView()
        
        
        if let url = Bundle.main.url(forResource: "covers", withExtension: "plist"),
            let contents = NSArray(contentsOf: url) as? [[String: String]] {
            covers = contents
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.isNavigationBarHidden = false
        collectionViewLayout.isFirstCellExcluded = true
        collectionViewLayout.isLastCellExcluded = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        collectionView.reloadData()
        collectionView.collectionViewLayout.invalidateLayout()
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return UIStatusBarAnimation.slide
    }
    
    //    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    //        guard segue.identifier == "ShowSettings" ,
    //                let settingsController = segue.destination as? SettingsController,
    //                let layout = collectionView.collectionViewLayout as? CollectionViewSlantedLayout else {
    //            return
    //        }
    //
    //        settingsController.collectionViewLayout = layout
    //    }
}

extension ViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return covers.count
    }
    
    func collectionView(_ collectionView: UICollectionView,cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as? CustomCollectionCell else {
            fatalError()
        }
        
        cell.image = UIImage(named: covers[indexPath.row]["picture"]!)!
        cell.configure(title: tiltles[indexPath.row])
        
        if let layout = collectionView.collectionViewLayout as? CollectionViewSlantedLayout {
            cell.contentView.transform = CGAffineTransform(rotationAngle: layout.slantingAngle)
        }
        
        return cell
    }
}

extension ViewController: CollectionViewDelegateSlantedLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        NSLog("Did select item at indexPath: [\(indexPath.section)][\(indexPath.row)]")
        
        let knowledgeView = self.storyboard?.instantiateViewController(withIdentifier: "KnowledgeViewController") as! KnowledgeViewController
        let TwoknowledgeView = self.storyboard?.instantiateViewController(withIdentifier: "TwoKnowledgeViewController") as! TwoKnowledgeViewController
        let ThirdknowledgeView = self.storyboard?.instantiateViewController(withIdentifier: "ThirdKnowledgeViewController") as! ThirdKnowledgeViewController
        let FourknowledgeView = self.storyboard?.instantiateViewController(withIdentifier: "FourKnowledgeViewController") as! FourKnowledgeViewController
        let FiftknowledgeView = self.storyboard?.instantiateViewController(withIdentifier: "FiftKnowledgeViewController") as! FiftKnowledgeViewController
        
        if indexPath.row == 0 {
            self.navigationController?.pushViewController(knowledgeView, animated: true)
        }
        else if indexPath.row == 1 {
            self.navigationController?.pushViewController(TwoknowledgeView, animated: true)
        }
        else if indexPath.row == 1 {
            self.navigationController?.pushViewController(ThirdknowledgeView, animated: true)
        }
        else if indexPath.row == 1 {
            self.navigationController?.pushViewController(FourknowledgeView, animated: true)
        }
        else {
            self.navigationController?.pushViewController(FiftknowledgeView, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: CollectionViewSlantedLayout, sizeForItemAt indexPath: IndexPath) -> CGFloat {
        return collectionViewLayout.scrollDirection == .vertical ? 275 : 325
    }
}

extension ViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard let collectionView = collectionView else {return}
        guard let visibleCells = collectionView.visibleCells as? [CustomCollectionCell] else {return}
        for parallaxCell in visibleCells {
            let yOffset = (collectionView.contentOffset.y - parallaxCell.frame.origin.y) / parallaxCell.imageHeight
            let xOffset = (collectionView.contentOffset.x - parallaxCell.frame.origin.x) / parallaxCell.imageWidth
            parallaxCell.offset(CGPoint(x: xOffset * xOffsetSpeed, y: yOffset * yOffsetSpeed))
        }
    }


    
    //ตัวแปรหน้าให้ความรู้
//    let pictures : [UIImage] =  [UIImage(named:"fruit-and-vagetable")!, UIImage(named:"famer")!, UIImage(named:"chemicals")!, UIImage(named:"chemicals")!]
//    let tiltles : [String] = ["เกษตรอินทรีย์คืออะไร", "ผักปลอดสารพิษ vs ผักเกษตรอินทรีย์", "ทำไมต้องบริโภคสินค้าเกษตรอินทรีย์", "พยากรณ์ NCDs"]
//    let myTitles : [String] = ["บทความ"]
//
//
//
//    //tableView หน้าให้ความรู้
//     @IBOutlet weak var tableView: UITableView!
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        //table header
//        func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//            return myTitles [section]
//        }
//    }
}
//แสดงผลtablecell และ การเปลี่ยนหน้่าไป detailViewController
//extension ViewController: UITableViewDelegate, UITableViewDataSource {
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 265
//    }
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return pictures.count
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "CellTableViewCell", for: indexPath) as! CellTableViewCell
//
//        cell.configure(picture: pictures[indexPath.row], title: tiltles[indexPath.row])
//
//        return cell
//    }

//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//
//        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//
//        let detailVC = storyBoard.instantiateViewController(withIdentifier: "DetailOneViewController") as! DetailViewController
//        let detailTwoVC = storyBoard.instantiateViewController(withIdentifier: "DetailViewTwoController") as! DetailViewController
//        let detailThreeVC = storyBoard.instantiateViewController(withIdentifier: "DetailViewThreeController") as! DetailViewController
//
//
//        if indexPath.row == 0 {
//        self.navigationController?.pushViewController(detailVC, animated: true)
//        }
//
//        else if indexPath.row == 1 {
//            self.navigationController?.pushViewController(detailTwoVC, animated: true)
//        }
//
//        else if indexPath.row == 2 {
//            self.navigationController?.pushViewController(detailThreeVC, animated: true)
//        }
//
//        else if indexPath.row == 3 {
//
//        }
//
//        else {
//
//        }
//    }
//}
